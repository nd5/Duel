/*
 * Title.cpp
 *
 *  Created on: 2015/04/14
 *      Author: Toida
 */

/******************************************************************************
 include
******************************************************************************/
#include "cocos2d.h"
//#include "SimpleAudioEngine.h"
#include "Title.h"
#include "GameMain.h"
#include "Option.h"
#include "Config.h"

USING_NS_CC;

CCScene* Title::scene()
{
	// 'scene' is an autorelease object
	CCScene *scene = CCScene::create();

	// 'layer' is an autorelease object
	Title *layer = Title::create();

	// add layer as a child to scene
	scene->addChild(layer);

	// return the scene
	return scene;
}

// on "init" you need to initialize your instance
bool Title::init()
{
	if ( !CCLayer::init() )
	{
		return false;
	}

	/* 画面サイズを取得する */
	CCSize winSize = CCDirector::sharedDirector()->getWinSize();
	CCSize visibleSize = CCDirector::sharedDirector()->getVisibleSize();
	CCPoint origin = CCDirector::sharedDirector()->getVisibleOrigin();

	/* アプリケーションを閉じるアイコンを作成する */
	CCMenuItemImage *pCloseItem = CCMenuItemImage::create(
														  "CloseNormal.png",
														  "CloseSelected.png",
														  this,
														  menu_selector(Title::menuCloseCallback));

	/* アイコン位置決め */
	pCloseItem->setPosition(ccp(origin.x + visibleSize.width - pCloseItem->getContentSize().width/2 ,
								origin.y + pCloseItem->getContentSize().height/2));

	// create menu, it's an autorelease object
	CCMenu* pMenu = CCMenu::create(pCloseItem, NULL);
	pMenu->setPosition(CCPointZero);
	this->addChild(pMenu, 1);

	/* ボタン表示用のメインレイヤー作成 */
	CCLayer *mainLayer = CCLayer::create();
	this->addChild(mainLayer);

	/* メニュー項目作成 */
	CCMenuItemFont::setFontName(MAIN_FONT);
	CCMenuItemFont::setFontSize(90 * WIN_SCALE);
	// CCSize bgSize = CCDirector::sharedDirector()->getWinSize();

	/* [メニュー]ゲームスタート */
	CCMenuItemFont* itemStartGame = CCMenuItemFont::create("Start Game", this, menu_selector(Title::GameMainStart));
	itemStartGame->setAnchorPoint(CCPoint(0.0, 0.5));

	/* [メニュー]オプション */
	CCMenuItemFont* itemOption = CCMenuItemFont::create("Option", this, menu_selector(Title::OptionStart));
	itemOption->setAnchorPoint(CCPoint(0.0, 0.5));

	/* メニュー表示 */
	CCMenu* menu = CCMenu::create();
	menu->addChild(itemStartGame);
	menu->addChild(itemOption);
	menu->alignItemsVerticallyWithPadding(50.0f * WIN_SCALE);
	menu->setPosition(ccp(winSize.width / 2 - 190 * WIN_SCALE, winSize.height / 2 - 20 * WIN_SCALE));

	menu->setColor(ccc3(255, 165, 0));
	mainLayer->addChild(menu, 1);

	/* Copyright 表示 */
	CCLabelTTF* labelCopyright = CCLabelTTF::create("CopyRight Test", CREDIT_FONT, 48 * WIN_SCALE);
	labelCopyright->setPosition(ccp(winSize.width / 2, 100 ));
	labelCopyright->setColor(ccc3(255, 255, 255));
	mainLayer->addChild(labelCopyright, 1);

	/* シングルタッチON */
	this->setTouchMode(kCCTouchesOneByOne);
	this->setTouchEnabled(true);

	/* バックキー、ホームキーを検出可能にする */
	this->setKeypadEnabled(true);

//	/////////////////////////////
//	// 3. add your codes below...
//
//	// add a label shows "Hello World"
//	// create and initialize a label
//
//    CCLabelTTF* pLabel = CCLabelTTF::create("Hello World", "Arial", 24);
//
//    // position the label on the center of the screen
//    pLabel->setPosition(ccp(origin.x + visibleSize.width/2,
//                            origin.y + visibleSize.height - pLabel->getContentSize().height));
//
//    // add the label as a child to this layer
//    this->addChild(pLabel, 1);
//
//    // add "HelloWorld" splash screen"
//    CCSprite* pSprite = CCSprite::create("HelloWorld.png");
//
//    // position the sprite on the center of the screen
//    pSprite->setPosition(ccp(visibleSize.width/2 + origin.x, visibleSize.height/2 + origin.y));
//
//    // add the sprite as a child to this layer
//    this->addChild(pSprite, 0);

	return true;
}

/* ゲームメイン画面遷移 */
void Title::GameMainStart()
{
	CCScene *gamemain = (CCScene *)GameMain::create();
	CCTransitionScene *tran = CCTransitionCrossFade::create(1.0, gamemain);
	CCDirector::sharedDirector()->replaceScene(tran);
}

/* ゲームメイン画面遷移 */
void Title::CharacterSelectStart()
{
//	CCScene *charasel = (CCScene *)CharacterSelect::create();
//	CCTransitionScene *tran = CCTransitionRotoZoom::create(1.0, charasel);
//	CCDirector::sharedDirector()->replaceScene(tran);
}

/* オプション画面遷移 */
void Title::OptionStart()
{
//	CCScene *option = (CCScene *)Option::create();
//	CCTransitionScene *tran = CCTransitionRotoZoom::create(1.0, option);
//	CCDirector::sharedDirector()->replaceScene(tran);
}

/* cocos2dxタッチ開始関数 */
//bool Title::ccTouchBegan(CCTouch *pTouch, CCEvent *pEvent)
bool Title::onTouchBegan(Touch *pTouch, Event *pEvent)
{
//	if( tapFlg ){
//		return false;
//	}
//	else{
//		tapFlg = true;
//	}

	/* タップポイント取得 */
	CCPoint location = this->convertTouchToNodeSpace(pTouch);

//	// タップ位置へパーティクルを移動
//	CCParticleSystemQuad *touchParticle = CCParticleSystemQuad::create(EXPLODINGRING_PARTICLE);
//	touchParticle->setPosition(location);
//	touchParticle->setAutoRemoveOnFinish(true);
//	this->addChild(touchParticle, 1);

	return true;
}

/* cocos2dxタッチ移動関数 */
//void Title::ccTouchMoved(CCTouch *pTouch, CCEvent *pEvent)
void onTouchMoved(Touch* pTouch, Event* pEvent)
{
	/* タップポイント取得 */
	CCPoint location = this->convertTouchToNodeSpace(pTouch);

//	// タップ位置へパーティクルを移動
//	CCParticleSystemQuad *touchParticle = CCParticleSystemQuad::create(TOUCH_PARTICLE);
//	touchParticle->setPosition(location);
//	touchParticle->setAutoRemoveOnFinish(true);
//	this->addChild(touchParticle, 1);
}

/* cocos2dxタッチ終了関数 */
//void Title::ccTouchEnded(CCTouch *pTouch, CCEvent *pEvent)
void Title::onTouchEnded(Touch *pTouch, Event *pEvent)
{
	/* タップポイント取得 */
	CCPoint location = this->convertTouchToNodeSpace(pTouch);

//	// タップ位置へパーティクルを移動
//	CCParticleSystemQuad *touchParticle = CCParticleSystemQuad::create(EXPLODINGRING_PARTICLE);
//	touchParticle->setPosition(location);
//	touchParticle->setAutoRemoveOnFinish(true);
//	this->addChild(touchParticle, 1);

//	// タップフラグ解除
//	tapFlg = false;
}

/* バックキー押下時処理 */
void Title::keyBackClicked()
{
	CCLOG("Title::keyBackClicked ================================");
	CCDirector::sharedDirector()->end();

#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
	/* iOS */
	exit(0);
#endif
}

/* ホームキー押下時処理 */
void Title::keyMenuClicked()
{
	CCLOG("Title::keyMenuClicked ================================");
}

/*  */
void Title::menuCloseCallback(CCObject* pSender)
{
    CCDirector::sharedDirector()->end();

#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    exit(0);
#endif
}
