#include "AppDelegate.h"
#include "Title.h"
#include "Config.h"
#include "cocos2d.h"
//#include "SimpleAudioEngine.h"

USING_NS_CC;

AppDelegate::AppDelegate() {

}

AppDelegate::~AppDelegate() 
{
}

bool AppDelegate::applicationDidFinishLaunching() {
	// initialize director
//	CCDirector* pDirector = CCDirector::sharedDirector();
//	CCEGLView* pEGLView = CCEGLView::sharedOpenGLView();
	auto pDirector  = Director::getInstance();
	auto pEGLView = pDirector->getOpenGLView();

	pDirector->setOpenGLView(pEGLView);

	/* �}���`���]�����[�V�����Ή� */
//	CCEGLView::sharedOpenGLView()->setDesignResolutionSize(BASE_WIDTH, BASE_HEIGHT, kResolutionNoBorder);
//	CCEGLView::sharedOpenGLView()->setDesignResolutionSize(pDirector->getWinSize().width, pDirector->getWinSize().height, kResolutionNoBorder);
	pDirector->getOpenGLView()->setDesignResolutionSize(pDirector->getWinSize().width, pDirector->getWinSize().height, kResolutionNoBorder);
	pDirector->setContentScaleFactor(BASE_WIDTH / pDirector->getWinSize().width);
//	CCLog("======================");
//	CCLog("�yWinSize�z X:%f y:%f", pDirector->getWinSize().width, pDirector->getWinSize().height);
//	CCLog("�yScaleFactor�z %f", pDirector->getContentScaleFactor());
//	CCLog("�yWIN_SCALE�z %f", WIN_SCALE);

	// turn on display FPS
	pDirector->setDisplayStats(true);

	// set FPS. the default value is 1.0/60 if you don't call this
	pDirector->setAnimationInterval(1.0 / 60);

	// create a scene. it's an autorelease object
	CCScene *pScene = Title::scene();

	// run
	pDirector->runWithScene(pScene);

	return true;
}

// This function will be called when the app is inactive. When comes a phone call,it's be invoked too
void AppDelegate::applicationDidEnterBackground() {
    CCDirector::sharedDirector()->stopAnimation();

    // if you use SimpleAudioEngine, it must be pause
    // SimpleAudioEngine::sharedEngine()->pauseBackgroundMusic();
}

// this function will be called when the app is active again
void AppDelegate::applicationWillEnterForeground() {
    CCDirector::sharedDirector()->startAnimation();

    // if you use SimpleAudioEngine, it must resume here
    // SimpleAudioEngine::sharedEngine()->resumeBackgroundMusic();
}
